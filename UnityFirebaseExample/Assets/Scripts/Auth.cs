﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Http;

public class Auth : MonoBehaviour
{
    // Thank you Evan Mulawski and the Stack Overflow community
    // https://stackoverflow.com/questions/4015324/how-to-make-http-post-web-request/4015346#4015346

    private static readonly HttpClient Client = new HttpClient();

    private static async void Response(Dictionary<string, string> values, string endpoint)
    {
        var content = new FormUrlEncodedContent(values);
        var response = await Client.PostAsync(endpoint, content);
        var responseString = await response.Content.ReadAsStringAsync();

        Debug.Log("RESPONSE: " + responseString);
    }

    public static void Login(string email, string password)
    {
        Debug.LogWarning(email);
        Debug.LogWarning(password);

        string endpoint = "https://us-central1-holon-studios.cloudfunctions.net/dummyLogin";

        var values = new Dictionary<string, string>
        {
            { "email", email },
            { "password", password }
        };

        //var content = new FormUrlEncodedContent(values);
        //var response = await Client.PostAsync(endpoint, content);
        //var responseString = await response.Content.ReadAsStringAsync();

        //Debug.Log("RESPONSE: " + responseString);

        Response(values, endpoint);
    }

    public static void Signup(string email, string password)
    {
        Debug.LogWarning(email);
        Debug.LogWarning(password);

        string endpoint = "https://us-central1-holon-studios.cloudfunctions.net/dummySignup";

        var values = new Dictionary<string, string>
        {
            { "email", email },
            { "password", password }
        };

        Response(values, endpoint);
    }
}
