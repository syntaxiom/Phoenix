﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SignupButton : MonoBehaviour
{
    public InputField email;
    public InputField password;

    void SignupOnClick()
    {
        Auth.Signup(email.text, password.text);
    }

    // Start is called before the first frame update
    void Start()
    {
        Button button = GetComponent<Button>();
        button.onClick.AddListener(SignupOnClick);
    }
}
