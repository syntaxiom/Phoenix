import * as functions from 'firebase-functions';
import * as firebase from 'firebase';
import * as admin from 'firebase-admin';

firebase.initializeApp({
  apiKey: "AIzaSyAkh6N2T-3qW0caMJldF5rpkOWfm7Ggomo",
  authDomain: "holon-studios.firebaseapp.com",
  databaseURL: "https://holon-studios.firebaseio.com",
  projectId: "holon-studios",
  storageBucket: "holon-studios.appspot.com",
  messagingSenderId: "577079214476",
  appId: "1:577079214476:web:e6de44e0dad1a3e6b3d6a9",
  measurementId: "G-B5W8XP1TXS"
});

admin.initializeApp();

const auth = firebase.auth();
const fs = admin.firestore();

// Start writing Firebase Functions
// https://firebase.google.com/docs/functions/typescript

// HTTP requests

export const helloWorld = functions.https.onRequest((request, response) => {
  response.send("Hello from Holon Studios!");
});

export const dummyLogin = functions.https.onRequest((req, res) => {
  const { username, password } = req.body;
  
  fs.collection('users').doc(username).get()
    .then((doc) => {
      const { email } = <any>doc.data();
      
      auth.signInWithEmailAndPassword(email, password)
        .then((user: any) => {
          res.json(user);
        })
        .catch((err: any) => {
          res.status(500).json(null);
        });
    });
})

// TODO : Make smarter
export const dummySignup = functions.https.onRequest((req, res) => {
  auth.createUserWithEmailAndPassword(req.body.email, req.body.password)
    .then((user: any) => {
      res.json(user);
    })
    .catch((err: any) => {
      res.json(null);
    });
})

// Triggers

export const makeUserDoc = functions.auth.user().onCreate((user) => {
  const userDocData = {
    alias: '(TBD)',
    email: user.email,
    roles: ['player']
  }
  
  fs.collection('users').doc(user.uid).set(userDocData);
})