import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // Angular default
  title = 'web';

  // **New**
  // "Users" experiment
  users: Observable<any[]>;

  constructor(fs: AngularFirestore) {
    this.users = fs.collection('users').valueChanges();
  }
}
