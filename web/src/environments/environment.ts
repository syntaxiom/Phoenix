// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAkh6N2T-3qW0caMJldF5rpkOWfm7Ggomo",
    authDomain: "holon-studios.firebaseapp.com",
    databaseURL: "https://holon-studios.firebaseio.com",
    projectId: "holon-studios",
    storageBucket: "holon-studios.appspot.com",
    messagingSenderId: "577079214476",
    appId: "1:577079214476:web:e6de44e0dad1a3e6b3d6a9",
    measurementId: "G-B5W8XP1TXS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
